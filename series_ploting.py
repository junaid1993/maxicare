
# It is to check weather time series is stationary or not Dicky Fuller test
# https://machinelearningmastery.com/time-series-data-stationary-python/

from pandas import Series
from matplotlib import pyplot
series = Series.from_csv('time_series.csv', header=0)
series.plot()
pyplot.show()