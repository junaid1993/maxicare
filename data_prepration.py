
import pandas as pd


# This function is to ignore exceptions
class trialContextManager:
    def __enter__(self): pass
    def __exit__(self, *args): return True
trial = trialContextManager()

#def column_name_formatter(s):
#    return s.lower().replace(' ', '_')

group_column_names = ['ADMISSIONDATE','HOSPITALNAME']
df = pd.read_csv('sample_dataset.csv')
# result = pd.DataFrame(columns=['ds'] + list(map(column_name_formatter, df["ICDGROUPINGDESC"].unique())))
result = pd.DataFrame(columns=['ds'] + list(df[group_column_names[1]].unique()))
result["ds"] = df[group_column_names[0]].unique()
column_names = list(result.columns.values)[1:] # we are ignoring first column because it's date
group = df.groupby(group_column_names).size()
for index, row in result.iterrows():
    for column in column_names:
        required_value = 0
        with trial: required_value = group[row["ds"], column]
        result[column][index] = required_value
result.to_csv("multivariate_wrt_hospital.csv", index = False)