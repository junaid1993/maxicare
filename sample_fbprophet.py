import pandas as pd
import numpy as np
from fbprophet import Prophet
from sklearn.metrics import mean_squared_error
from math import sqrt
import matplotlib.pyplot as plt
from matplotlib import rc
from datetime import datetime
import statsmodels.api as sm
import datetime as dt


# Use this Data Set for overall trend
#df = pd.read_csv('time_series.csv')
#date_format = "%d/%m/%Y"

# Hospital Wise Trend
#hospital = pd.read_csv('multivariate_wrt_hospital.csv')
#df = pd.DataFrame(columns=['ds', 'MYHEALTH CLINIC-SM NORTH EDSA'])
#df["ds"] = hospital["ds"]
#df["y"] = hospital["MYHEALTH CLINIC-SM NORTH EDSA"]
#date_format = '%m/%d/%Y'


disease = pd.read_csv('multivariate_wrt_disease.csv')
df = pd.DataFrame(columns=['ds', 'Renal Failure'])
df["ds"] = disease["ds"]
df["y"] = disease["Renal Failure"]
date_format = '%m/%d/%Y'


df["ds"] = [datetime.strptime(date, date_format) for date in df["ds"]]
decomposed = sm.tsa.seasonal_decompose(np.array(df["y"]), freq=3)
df["y"] = decomposed.resid

#split_index = int(len(df) - (len(df) * 0.02))
#train = df.head(split_index)
#est = df.tail(len(df) - split_index)
#testing_data_for_model = pd.DataFrame(test["ds"], columns=["ds"])

train_start_arr = ["01/01/2017", "08/01/2017", "15/01/2017", "22/01/2017"]
train_end_arr = ["30/11/2017", "7/12/2017", "14/12/2017", "21/12/2017"]
test_start_arr = ["01/12/2017", "08/12/2017", "15/12/2017", "22/12/2017"]
test_end_arr = ["07/12/2017", "14/12/2017", "21/12/2017", "28/12/2017"]


rc('text', usetex= False)
rc('xtick', labelsize=12) 
rc('ytick', labelsize=14)
rc('text',fontsize = 12)
plt.figure(figsize=(14,8))
x = [0,1,2,3,4,5,6]

i = 0
for i in range(4):
    train_start = datetime.strptime(train_start_arr[i], '%d/%m/%Y')
    train_end = datetime.strptime(train_end_arr[i], '%d/%m/%Y')
    test_start = datetime.strptime(test_start_arr[i], '%d/%m/%Y')
    test_end = datetime.strptime(test_end_arr[i], '%d/%m/%Y')
    
    train = df.loc[(df["ds"] >= train_start) & (df["ds"] <= train_end)]
    test = df.loc[(df["ds"] >= test_start) & (df["ds"] <= test_end)]
    testing_data_for_model = pd.DataFrame(test["ds"], columns=["ds"])
    
    
    #m = Prophet(holidays=holidays)
    m = Prophet()
    #m.add_seasonality(name='weekly', period=7, fourier_order=3, prior_scale=0.1);
    m.fit(train)
    
    future = m.make_future_dataframe(periods=37, freq="D", include_history = False)
    forecast = m.predict(testing_data_for_model)
    dates = forecast["ds"]
    predictions = forecast["yhat"]
    actual = test["y"]
    
    rms = sqrt(mean_squared_error(actual, predictions))
    #difference = abs(actual - predictions)
    print("Root Mean Square Error: ", rms)
    
    plt.subplot(2,2,(i+1))
    plt.title("Week " + str(i+1))
    plt.plot(x,actual.values, lw = 3, label = 'Actual', zorder = -1)
    plt.plot(x,predictions.values, lw = 3, label = 'Predicted', zorder = -1)
    plt.scatter(x,actual.values,s=300,color='b', alpha = 1)
    plt.scatter(x,predictions.values,s=300,color='r', alpha = 1)
    
    my_xticks = [dt.datetime.date(date) for date in dates]
    plt.xticks(x, my_xticks)
    plt.xticks(rotation=35)
    plt.ylabel('Number of Patients')
    plt.xlabel('Date')
    #plt.title('No of users distributed by the amount of items they have bought')
    plt.legend(loc="lower left")
    plt.tight_layout()
plt.savefig('Disease')