
# This file is to build FB prophet model for a specific hospital



import pandas as pd
import matplotlib.pyplot as plt
from fbprophet import Prophet
import statsmodels.api as sm
import numpy as np

hospital = pd.read_csv('multivariate_wrt_hospital.csv')
dataset = pd.DataFrame(columns=['ds', 'MYHEALTH CLINIC-SM NORTH EDSA'])
dataset["ds"] = hospital["ds"]
dataset["y"] = hospital["MYHEALTH CLINIC-SM NORTH EDSA"]

decomposed = sm.tsa.seasonal_decompose(np.array(dataset["y"]), freq=3)
dataset["y"] = decomposed.resid

split_index = int(len(dataset) - (len(dataset) * 0.02))
train = dataset.head(split_index)
test = dataset.tail(len(dataset) - split_index)
testing_data_for_model = pd.DataFrame(test["ds"], columns=["ds"])

m = Prophet()
#m.add_seasonality(name='weekly', period=7, fourier_order=3, prior_scale=0.1);
m.fit(train)

forecast = m.predict(testing_data_for_model)
dates = forecast["ds"]
predictions = forecast["yhat"]
actual = test["y"]

line_up, = plt.plot(dates, predictions, label='predictions')
line_down, = plt.plot(dates, actual, label='Actual')
plt.legend(handles=[line_up, line_down])
plt.show()






print(decomposed.trend)
print(decomposed.seasonal)
print(decomposed.resid)
print(decomposed.observed)

dta = pd.Series([x%3 for x in range(100)])