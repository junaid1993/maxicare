#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 27 17:16:18 2018

@author: Junaid
"""


# This file is to build FB prophet model for a specific hospital



import pandas as pd
import matplotlib.pyplot as plt
from fbprophet import Prophet
import statsmodels.api as sm
import numpy as np
import plotly.plotly as py
import plotly.graph_objs as go
from ggplot import *
import datetime as df

disease = pd.read_csv('multivariate_wrt_disease.csv')
dataset = pd.DataFrame(columns=['ds', 'Renal Failure'])
dataset["ds"] = disease["ds"]
dataset["y"] = disease["Renal Failure"]

decomposed = sm.tsa.seasonal_decompose(np.array(dataset["y"]), freq=3)

#print(decomposed.trend)
#print(decomposed.seasonal)
#print(decomposed.resid)
#print(decomposed.observed)

dataset["y"] = decomposed.resid

split_index = int(len(dataset) - (len(dataset) * 0.02))
train = dataset.head(split_index)
test = dataset.tail(len(dataset) - split_index)
testing_data_for_model = pd.DataFrame(test["ds"], columns=["ds"])

m = Prophet()
#m.add_seasonality(name='weekly', period=7, fourier_order=3, prior_scale=0.1);
m.fit(train)

forecast = m.predict(testing_data_for_model)
dates = forecast["ds"]
predictions = forecast["yhat"]
actual = test["y"]

#line_up, = plt.plot(dates, predictions, label='predictions')
#line_down, = plt.plot(dates, actual, label='Actual')
#plt.legend(handles=[line_up, line_down])
#plt.show()

    
ploting_df = pd.DataFrame(columns = ["dates", "ground_truth", "predictions"])
ploting_df["dates"] = dates
ploting_df["ground_truth"] = actual
ploting_df["predictions"] = predictions

print(ggplot(aes(x='dates'), data = ploting_df) +\
    geom_line(aes(y='ground_truth'), color='blue') +\
    geom_line(aes(y='predictions'), color='red')) 




dta = pd.Series([x%3 for x in range(100)])





import matplotlib.pyplot as plt
from matplotlib import rc

rc('text', usetex= False)
rc('xtick', labelsize=12) 
rc('ytick', labelsize=14)
rc('text',fontsize = 20)

plt.figure(figsize=(14,8))
x = [0,1,2,3,4,5,6,7]

plt.plot(x,actual.values, lw = 3, label = 'Actual', zorder = -1)
plt.plot(x,predictions.values, lw = 3, label = 'Predicted', zorder = -1)
plt.scatter(x,actual.values,s=300,color='b', alpha = 1)
plt.scatter(x,predictions.values,s=300,color='r', alpha = 1)

my_xticks = [dt.datetime.date(date) for date in dates]
plt.xticks(x, my_xticks)
plt.xticks(rotation=35)
plt.ylabel('Number of Patients')
plt.xlabel('Date')
#plt.title('No of users distributed by the amount of items they have bought')
plt.legend()
plt.tight_layout()
plt.savefig('hypertension_1_to_7')
plt.show()


